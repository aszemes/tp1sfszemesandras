<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DefaultController extends AbstractController
{
    #[Route('', name: 'app_homepage')]
    public function index(): Response
    {
        return $this->render('default/index.html.twig', []);
    }

    #[Route("/produits/{id}",requirements:["id"=> "\d*"], name:"app_produits")]
    public function produits($id): Response
    {
        $src = '';
        if ($id == 1) {
            $src = 'images/laptop.png';
        } else if ($id == 2) {
            $src = 'images/ipad.png';
        } else if ($id == 3) {
            $src = 'images/drawing.png';
        }

        return $this->render('default/produits.html.twig', [
            "id" => $id,
            "src" => $src,
        ]);
    }

    #[Route('/histoire', name: 'histoire_page')]
    public function histoire(): Response
    {
        return $this->render('default/histoire.html.twig', []);
    }
}
